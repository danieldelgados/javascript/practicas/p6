 window.addEventListener("load",()=>{
 	var fotos=["mountain","river","road","waterfall","xtrees","pink","green","marge"];
 	var imagenes=[];
	var aux=0;
 	for(var c=0;c<fotos.length;c++){
 		imagenes[c]=new Image();
 		imagenes[c].src="imgs/nature/" +fotos[c]+ ".jpg";
 	}
 	
 	for(var c=0;c<4;c++){
 		document.querySelectorAll(".foto")[c].src="imgs/thumbs/nature/" +fotos[c]+ ".jpg";
 	}

 	for(var c=0;c<4;c++){

		document.querySelectorAll(".foto")[c].addEventListener("click",()=>{
			var indice=event.target.getAttribute("data-indice");			
			var gallery=document.querySelector(".galeria");
			gallery.style.backgroundImage="url('"+imagenes[indice].src+"')";
			
		});			
	};

	document.querySelector('.siguiente').addEventListener('click',()=>{
			  aux+=1;
			 if (aux>=imagenes.length) {
			 	aux=0;
			 };				 
			 slide(aux)
			 
			
		});	
	
	document.querySelector('.anterior').addEventListener('click',()=>{
		aux--;
		if(aux==-1){
			aux=3;
		};
		slide(aux)

	});
	
	/* numeros links parte baja  */
	var num=document.querySelector('.numeros');
	var pags=Math.ceil(fotos.length/4);

	for(var i=0;i<pags;i++){
		num.innerHTML+="<a href='#' data-indice="+i+">"+(i+1)+"</a>";
	};
	
	/* cargar mas fotos al pinchar en los numeros */
		var links=document.querySelectorAll('.numeros a');

		links.forEach((item) => {
		  item.addEventListener('click',()=>{
		  	var indice=event.target.getAttribute("data-indice");
		  	var uno=[];
		  	var dos=[];
		  	for(var i=0;i<fotos.length-4;i++){
		  		uno.push(fotos[i]);
		  	};
		  	for(var i=4;i<fotos.length;i++){
		  		dos.push(fotos[i]);
		  	};
		  	console.log(uno);
		  	console.log(dos);
		  	if(indice==0){
		  		for(var c=0;c<uno.length;c++){

 					document.querySelectorAll(".foto")[c].src="imgs/thumbs/nature/" +uno[c]+ ".jpg";
 				};
		  	}else if(indice==1){
		  		for(var c=0;c<dos.length;c++){
 					document.querySelectorAll(".foto")[c].src="imgs/thumbs/nature/" +dos[c]+ ".jpg";
 				};
		  	};
		  	
		  });
		})
	
	
 	document.querySelector(".galeria").style.backgroundImage="url(./imgs/thumbs/nature/"+fotos[0]+ ".jpg)";
 	
 	for(var c=0;c<fotos.length;c++){
 		document.querySelector('.zoom').addEventListener('click',()=>{
 		var caja=document.querySelector(".ocultar");
 		var gallery=document.querySelector(".galeria");
 		caja.style.backgroundImage=gallery.style.backgroundImage;
 		caja.style.display="block";

 		});
	};

 	document.querySelector(".ocultar").style.display="none";
 	document.querySelector(".ocultar").addEventListener("click",()=>{
		var caja=document.querySelector(".ocultar");
		caja.style.display="none";
		caja.style.backgroundImage="none";
	});
 	
 	function slide(r){
 		var indice=event.target.getAttribute("data-indice");
		var gallery=document.querySelector(".galeria");		
						
		gallery.style.backgroundImage="url('"+imagenes[r].src+"')";
 	};

 	});


 // function mover(arg) {
 // 	 var numero=document.createElement('li');
 //        numero.className='slide';
 //        document.querySelector('.num').appendChild(numero);
 //        var link=document.createElement('a');
	// 	link.setAttribute('href', arg);
	// 	document.querySelector('.slide').appendChild(link);
 // };

